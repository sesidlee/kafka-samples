package com.devcode.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

@RestController
public class ImportController {
    private static final Logger LOG = LoggerFactory.getLogger(ImportController.class);

    @Autowired
    private KafkaImporter kafkaImporter;

    /**
     * receives a message with the format path=<path to file>
     * @param message
     * @return
     */
    @PostMapping("/import")
    public String importFromPath(@RequestBody String message) {
        LOG.info("Got message {}", message);
        String path = null;
        try {
            path = URLDecoder.decode(message, "UTF-8");
            path = path.split("=")[1];
            LOG.info("path: {}", path);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "error";
        }

        try (Stream<String> stream = Files.lines(Paths.get(path))) {
            stream.forEach(kafkaImporter::send);
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
        return "ok";
    }
}
