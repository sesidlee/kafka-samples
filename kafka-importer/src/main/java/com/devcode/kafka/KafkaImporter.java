package com.devcode.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
;

@Component
public class KafkaImporter {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaImporter.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${app.topic}")
    private String topic;

    public void send(String payload) {
        kafkaTemplate.send(topic, payload);
        LOG.info("Message: "+payload+" sent to topic: "+topic);
    }
}
