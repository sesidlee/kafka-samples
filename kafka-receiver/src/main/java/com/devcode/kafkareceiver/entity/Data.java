package com.devcode.kafkareceiver.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "bycycle_data")
public class Data {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name ="starttime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Column(name ="endtime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    private int startStationId;

    private float startStationLatitude;

    private float startStationLongitude;

    private int endStationId;

    private float endStationLatitude;

    private float endStationLongitude;

    private float distance;

    private int duration;

    private int bikeId;

    private String userType;

    @Column(name= "member_birth_year")
    private int birthYear;

    @Column(name= "member_gender")
    private String gender;

    @Column(name= "bike_share_for_all_trip")
    private boolean bikeShare;

    public Data() {
    }

    public Data(Date startTime, Date endTime, int startStationId, float startStationLatitude, float startStationLongitude, int endStationId, float endStationLatitude, float endStationLongitude, float distance, int duration, int bikeId, String userType, int birthYear, String gender, boolean bikeShare) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.startStationId = startStationId;
        this.startStationLatitude = startStationLatitude;
        this.startStationLongitude = startStationLongitude;
        this.endStationId = endStationId;
        this.endStationLatitude = endStationLatitude;
        this.endStationLongitude = endStationLongitude;
        this.distance = distance;
        this.duration = duration;
        this.bikeId = bikeId;
        this.userType = userType;
        this.birthYear = birthYear;
        this.gender = gender;
        this.bikeShare = bikeShare;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getStartStationId() {
        return startStationId;
    }

    public void setStartStationId(int startStationId) {
        this.startStationId = startStationId;
    }

    public float getStartStationLatitude() {
        return startStationLatitude;
    }

    public void setStartStationLatitude(float startStationLatitude) {
        this.startStationLatitude = startStationLatitude;
    }

    public float getStartStationLongitude() {
        return startStationLongitude;
    }

    public void setStartStationLongitude(float startStationLongitude) {
        this.startStationLongitude = startStationLongitude;
    }

    public int getEndStationId() {
        return endStationId;
    }

    public void setEndStationId(int endStationId) {
        this.endStationId = endStationId;
    }

    public float getEndStationLatitude() {
        return endStationLatitude;
    }

    public void setEndStationLatitude(float endStationLatitude) {
        this.endStationLatitude = endStationLatitude;
    }

    public float getEndStationLongitude() {
        return endStationLongitude;
    }

    public void setEndStationLongitude(float endStationLongitude) {
        this.endStationLongitude = endStationLongitude;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getBikeId() {
        return bikeId;
    }

    public void setBikeId(int bikeId) {
        this.bikeId = bikeId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isBikeShare() {
        return bikeShare;
    }

    public void setBikeShare(boolean bikeShare) {
        this.bikeShare = bikeShare;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
