package com.devcode.kafkareceiver.repository;

import com.devcode.kafkareceiver.entity.Data;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataRepository extends CrudRepository<Data, Long> {
}
