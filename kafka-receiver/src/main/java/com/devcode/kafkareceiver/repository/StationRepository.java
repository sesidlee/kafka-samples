package com.devcode.kafkareceiver.repository;

import com.devcode.kafkareceiver.entity.Station;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StationRepository extends CrudRepository<Station, Long> {
    public Station findByName(String name);
}
