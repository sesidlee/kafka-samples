package com.devcode.kafkareceiver;

import com.devcode.kafkareceiver.entity.Data;
import com.devcode.kafkareceiver.entity.Station;
import com.devcode.kafkareceiver.repository.DataRepository;
import com.devcode.kafkareceiver.repository.StationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Component
public class MessageTransformer {
    private static final Logger LOG = LoggerFactory.getLogger(MessageTransformer.class);
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSS");

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private DataRepository dataRepository;

    public void transform(String message) {
        String[] splitMessage = message.split(",");
        if (splitMessage.length != 16) {
            LOG.error("Incorrect number of columns: {}", splitMessage.length);
            return;
        }

        Optional<Integer> startStation = findStationId(splitMessage[3], splitMessage[4]);
        Optional<Integer> endStation = findStationId(splitMessage[7], splitMessage[8]);
        float distance = calculateDistance(splitMessage);

        Data saveData = new Data(parseDateString(splitMessage[1]),
                parseDateString(splitMessage[2]),
                startStation.orElse(-1),
                parseFloat(splitMessage[5]),
                parseFloat(splitMessage[6]),
                endStation.orElse(-1),
                parseFloat(splitMessage[9]),
                parseFloat(splitMessage[10]),
                distance,
                parseInt(splitMessage[0]),
                parseInt(splitMessage[11]),
                parseString(splitMessage[12]),
                parseInt(splitMessage[13]),
                parseString(splitMessage[14]),
                parseBikeShare(splitMessage[15])
                );
        dataRepository.save(saveData);
    }

    private Optional<Integer> findStationId(String stationId, String stationName) {
        stationName = stationName.replace("\"", "");
        if (!isEmpty(stationId) && !isEmpty(stationName)) {
            Station station = stationRepository.findByName(stationName);
            if (station == null) {
                Station newStation = new Station(Integer.parseInt(stationId), stationName);
                Station savedStation = stationRepository.save(newStation);
                return Optional.of(savedStation.getId());
            } else {
                return Optional.of(station.getId());
            }
        }
        return Optional.empty();
    }

    private Date parseDateString(String input) {
        input = parseString(input);
        try {
            return formatter.parse(input);
        } catch (ParseException e) {
            LOG.error("Failed to parse string: {}", input, e);
            return new Date();
        }
    }

    private int parseOptional(Optional<Integer> input) {
        return input.orElse(-1);
    }

    private boolean isEmpty(String input) {
        input = parseString(input);
        return input.isEmpty() || "null".equalsIgnoreCase(input);
    }

    private float calculateDistance(String[] splitMessage) {
        return distFrom(Float.parseFloat(splitMessage[5]), Float.parseFloat(splitMessage[6]), Float.parseFloat(splitMessage[9]), Float.parseFloat(splitMessage[10]));
    }

    private int parseInt(String input) {
        if (!isEmpty(input)) {
            return Integer.parseInt(input);
        } else {
            return 0;
        }
    }

    private boolean parseBikeShare(String input) {
        if (!isEmpty(input) && "\"yes\"".equalsIgnoreCase(input)) {
            return true;
        } else {
            return false;
        }
    }

    private float parseFloat(String input) {
        if (!isEmpty(input)) {
            return Float.parseFloat(input);
        } else {
            return new Float(0);
        }
    }

    private String parseString(String input) {
        return input.replace("\"", "");
    }

    private float distFrom(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }
}
