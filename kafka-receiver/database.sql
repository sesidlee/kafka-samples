create database kafka;

CREATE USER 'kafka'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON * . * TO 'kafka'@'localhost';
FLUSH PRIVILEGES;

use kafka;

create table IF NOT EXISTS stations(
    id INT NOT NULL PRIMARY KEY,
    name varchar(200)
);

create table IF NOT EXISTS bycycle_data(
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    starttime datetime,
    endtime datetime,
    start_station_id int DEFAULT NULL,
    start_station_latitude decimal,
    start_station_longitude decimal,
	end_station_id int null,
	end_station_latitude decimal,
	end_station_longitude decimal,
	distance decimal,
	bike_id int DEFAULT NULL,
	user_type varchar(20) DEFAULT NULL,
	member_birth_year int DEFAULT NULL,
	member_gender varchar(7) DEFAULT NULL,
	bike_share_for_all_trip boolean,
	duration int
);