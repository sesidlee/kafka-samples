för att importera en csv fil så kan du köra curl med en sökväg till en csv fil.
sökvägen måste vara path=<filnamn>
t.ex.
curl -X POST -d "path=%2Fhome%2Fdaniel%2FDownloads%2Ftest.csv" http://localhost:8090/import

namn på topic kan konfigureras i main/java/resources/application.properties, konfig "app.topic"
